"""
*   Author:     Sipho Mkhwanazi
*   Company:    Weather Inteligence Systems
*   Purpose:    The script was written for purposes of automating sending insurance ingestion data
*   Date:       2017/11/10
*   Version:    1.0.2
"""
import os, time
import glob
import datetime as date
import ftplib
import logging as log
from xml.etree import ElementTree as ET

class ftpDispatcher:

	src = []
	dest = []
	newFiles = []

	def __init__(self):
		log.basicConfig(filename='ftpDispatcher.log', filemode='a', format='%(asctime)s: %(name)s - %(levelname)s: %(message)s', datefmt='%Y-%m-%d %I:%M:%S', level=log.DEBUG)
		self.destinations = os.path.abspath(os.path.join('destinations.xml'))
		self.sources = os.path.abspath(os.path.join('sources.xml'))
		self.destinationsList = ET.parse(self.destinations)
		self.sourcesList = ET.parse(self.sources)
		self.dateTime1Ext = date.datetime.today().strftime('_%Y%m01.csv')
		self.dateTime2Ext = date.datetime.today().strftime('_%Y%m01_%Y%m01.csv')
		log.debug('ftpDispatcher instance invoked with sources {0} and destinations {1}\n'.format(self.sources, self.destinations))

	def get_latest_files(self, file_path, file_name, project_name, type, file_glob, clean_up):

		path = file_path
		filename = file_name
		fileExtension = filename.rsplit('.',1)[1]
		log.debug(("File path is -> {0}\n").format(file_path))
		log.info(('found {0} in sources with path: {1}\n').format(type, path))

		filename = glob.glob(path+'/'+file_glob+'.'+fileExtension)
		if (len(filename) > 0):
			filename = max(filename,key=os.path.getctime)
			filename = filename.rsplit('/',1)[1]
			name = filename
			filename = os.path.abspath(os.path.join(path,filename))
			log.info(('found file named: {0} of type {1} using glob filter {2}\n').format(filename, type, file_glob))
			source = {
				'name': project_name
				,'path': path
				,'filename': filename
				,'savefilename': name
				,'clean_up': clean_up
			}

		else:
			log.debug('No latest {0} files found with glob filter {1}..\n'.format(type,file_glob))

		if 'source' in locals():
			if len(source) > 0:
				self.src.append(source)
				return True

		log.debug(("No new files changed for project name {1} in path {0}").format(path, type))
		return False

	"""
	The following method handles finding multiple files that where created/changed
	in the past x seconds
	"""
	def get_multi_type_files(self, file_path, project_name, clean_up):

		path = file_path

		seconds = 600

		for root, dirs, files in os.walk(path):

			for name in files:

				#log.info(("Checking {0} mtime is {1} > 600\n").format((os.path.join(root, name)), str(time.time() - os.stat(os.path.join(root, name)).st_mtime)))

				if (time.time() - os.stat(os.path.join(root, name)).st_ctime) < seconds:

					filename = os.path.abspath(os.path.join(root, name))

					source = {
						'name': project_name
						,'path': root
						,'filename': filename
						,'savefilename': name
						,'clean_up': clean_up
					}
					self.src.append(source)

		if 'source' in locals():
			if len(source) > 0:
				return True

		log.debug("No new files changed in the past {2} for project name {1} in path {0}".format(path, project_name, str(seconds)))
		return False

	"""
	This method deals with monthly insurance ingestion files
	"""
	def get_current_month_base(self, path, full_path_name, project_name, clean_up):

		if os.path.isfile(full_path_name):
			name = project_name+self.dateTime1Ext
			filename = full_path_name
			log.info(('found {0}\n').format(filename))
			source = {
				'name': project_name
				,'path': path
				,'filename': filename
				,'savefilename': name
				,'type': 'InsuranceIngestion'
				,'clean_up': clean_up
			}

			if len(source) > 0:
				self.src.append(source)
				return True
		else:
			log.info('No such file named -> {0}\n'.format(full_path_name))
		return False

	"""
	This method will get all sources from the sources xml conf file
	"""
	def getSources(self):

		log.debug('getSources invoked now looking for sources..\n')

		sources = self.sourcesList.getroot()

		for source in sources.findall('source'):

			project_name = source.find('name').text

			type = source.find('type').text

			file_glob = source.find('regex').text

			clean_up = source.find('clear_after_transfer').text

			path = source.find('path').text

			filename = source.find('filename').text

			log.info('found source type => {0}\n'.format(type))

			if type == 'InsuranceIngestion':

				f1 = os.path.join(path,project_name+self.dateTime1Ext)

				f2 = os.path.join(path,project_name+self.dateTime2Ext)

				log.info('found {0} in sources with path: {1}\n'.format(type, path))

				if self.get_current_month_base(path=path, full_path_name=f1, project_name=project_name, clean_up=clean_up):
					log.debug("Found the following..")
					log.debug(self.src)
				elif self.get_current_month_base(path=path, full_path_name=f2, project_name=project_name, clean_up=clean_up):
					log.debug("Found the following..")
					log.debug(self.src)
				else:
					log.info('Could not find either {0} or {1}\n'.format(f1, f2))

			elif type == 'Multi':

				"""
				The following gets all latest files created in the last 10 mins or 600 secs
				If found these files will be uploaded immediately
				"""
				log.info("Found source type => {0} upload for {1}..\n".format(type, source.find('name').text))

				result = self.get_multi_type_files(file_path=path, project_name=project_name, clean_up=clean_up)

				if result:
					log.debug("Found the following..")
					log.debug(self.src)
				else:
					log.debug("No new files found")

			else:

				result = self.get_latest_files(file_path=path,file_name=filename, project_name=project_name, type=type,file_glob=file_glob, clean_up=clean_up)

				if result:
					log.debug("Found the following..")
					log.debug(self.src)
				else:
					log.debug("No new files found")
		pass


	"""
	This method extracts destiantion parameters
	from the destinations xml config file
	"""
	def getDestinations(self):

		log.info('getDestinations starting..\n')

		destinations = self.destinationsList.getroot()

		for destination in destinations.findall('destination'):

			d = {
				'name': destination.find('name').text
				,'protocol':  destination.find('protocol').text
				,'address': destination.find('address').text
				,'port': destination.find('port').text
				,'root_dir': destination.find('root_dir').text
				,'username': destination.find('username').text
				,'password': destination.find('password').text
			}

			self.dest.append(d)

		log.info('found some destinations: ')
		log.info(self.dest)

		pass

	"""
	This method handles the actual transfer of files
	For insurance ingestion, base files will be deleted immediately upon
	successfull transfer
	"""
	def uploadFile(self):

		log.info('uploadFile starting\n')

		self.getSources()

		if len(self.src) > 0:
			self.getDestinations()
		else:
			return False

		for file in self.src:

			for destination in self.dest:

				if file['filename']:

					if destination['name'] == file['name']:

						filename = file['filename']
						protocol = destination['protocol']
						port = destination['port']
						host = destination['address']
						root_dir = destination['root_dir']
						username = destination['username']
						password = destination['password']
						log.info('Connection params are: protocol: {0}; host: {1}; port: {2}; username: {3}; passwd: {4}\n'.format(protocol, host, port, username, password))

						if protocol == 'ftp':

							try:
								#print(host, port, protocol, username, password)

								with ftplib.FTP(host) as ftp:

									ftp.login(username, password)

									ftp.cwd(root_dir)

									#log.info(ftp.nlst())

									if file['savefilename'] not in ftp.nlst():

										log.info('filename -> {0} name {1} savefilename => {2}\n'.format(filename, file['name'], file['savefilename']))

										response = ftp.storbinary('STOR ' +file['savefilename'],open(filename,'rb'))+' => '+filename

										log.debug(response)

										#Delete after successfull Transfer
										if '226 Transfer OK' in response:
											if 'clean_up' in file:
												if file['clean_up'] == 'Yes':
													log.debug("Begining to delete -> {} file\n".format(filename))
													os.remove(filename)

									else:

										log.info(filename+' file already exists in destination\n')

							except ftplib.error_reply as e:
								log.error('Unable to connect {0}\n'.format(e))
								pass
						else:
							log.debug("Unfortunately we do not support the {0} protocol. Please request this method.\n".format(protocol))
				else:

					log.error('No file name to save => {0}\n'.format(file['savefilename']))



x = ftpDispatcher()

x.uploadFile()